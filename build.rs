use image::DynamicImage;
use lazy_static::lazy_static;
use std::env::var_os;
use std::error::Error;
use std::fs::{read, write};
use std::path::{Path, PathBuf};

lazy_static! {
    static ref OUT_DIR: PathBuf = PathBuf::from(var_os("OUT_DIR").unwrap());
}

macro_rules! convert_xbm_nekos {
    ($($name:ident)*) => {
        $(
            convert_xbm_neko(stringify!($name))?;
        )*
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    convert_xbm_nekos! {
        awake
        down1 down2
        dtogi1 dtogi2
        dwleft1 dwleft2
        dwright1 dwright2
        jare2
        kaki1 kaki2
        left1 left2
        ltogi1 ltogi2
        mati2 mati3
        right1 right2
        rtogi1 rtogi2
        sleep1 sleep2
        up1 up2
        upleft1 upleft2
        upright1 upright2
        utogi1 utogi2
    };
    Ok(())
}

fn convert_xbm_neko(
    name: &str
) -> Result<(), Box<dyn Error>> {
    const BMP_PREFIX: &str = "assets/bitmaps/neko";
    const BMS_PREFIX: &str = "assets/bitmasks/neko";

    // There's probably a better way to do this!
    let mut name_mask = String::from(name);
    name_mask.push_str("_mask");

    let mut bmp_path = (BMP_PREFIX.as_ref() as &Path).join(name);
    let mut bms_path = (BMS_PREFIX.as_ref() as &Path).join(&name_mask);
    bmp_path.set_extension("xbm");
    bms_path.set_extension("xbm");
    let dest_path = &OUT_DIR.join(name);

    let dimensions = convert_xbm_parts(
        name, bmp_path, &name_mask, bms_path, dest_path
    )?;
    assert_eq!(dimensions, (32, 32));
    Ok(())
}

fn convert_xbm_parts(
    bmp_name: &str, bmp_path: impl AsRef<Path>,
    bms_name: &str, bms_path: impl AsRef<Path>,
    dest_path: impl AsRef<Path>
) -> Result<(u32, u32), Box<dyn Error>> {
    let mut bmp = open_xbm(bmp_name, bmp_path)?.into_rgba();
    let bms = {
        let mut x = open_xbm(bms_name, bms_path)?;
        x.invert();
        x.into_luma()
    };
    for (pixel, mask) in bmp.pixels_mut().zip(bms.pixels()) {
        pixel[3] = mask[0];
    }
    let (width, height) = bmp.dimensions();
    write(
        dest_path,
        bmp
            .into_flat_samples()
            .image_slice()
            .ok_or("image crate has a bug")?
    )?;
    Ok((width, height))
}

fn open_xbm(
    name: &str,
    path: impl AsRef<Path>
) -> Result<DynamicImage, Box<dyn Error>> {
    let data = read(path)?;
    xbm::parse(name, &data)
}

mod xbm {
    use core::convert::TryFrom;
    use image::{DynamicImage, ImageBuffer};
    use pom::parser::{Parser, end, one_of, sym, seq};
    use std::error::Error;

    fn macro_space<'a>() -> Parser<'a, u8, ()> {
        one_of(b" \t").repeat(1..).discard()
    }
    fn whitespace<'a>() -> Parser<'a, u8, ()> {
        // TODO: Add comment support if necessary
        one_of(b" \t\r\n").repeat(1..).discard()
    }
    fn integer<'a>() -> Parser<'a, u8, u32> {
        let decimal = one_of(b"123456789") + one_of(b"0123456789").repeat(..);
        let decival = decimal
            .map(|(c, mut v)| { v.insert(0, c); v })
            .convert(String::from_utf8)
            .convert(|s| u32::from_str_radix(&s, 10));
        let octal = sym(b'0') * one_of(b"01234567").repeat(..);
        let octval = octal
            .convert(String::from_utf8)
            .convert(|s| u32::from_str_radix(&s, 8));
        let hex = seq(b"0x") * one_of(b"0123456789ABCDEFabcdef").repeat(1..);
        let hexval = hex
            .convert(String::from_utf8)
            .convert(|s| u32::from_str_radix(&s, 16));
        sym(b'+').opt() * (decival | octval | hexval)
    }
    fn define<'a>(name: &'a [u8], attr: &'a [u8]) -> Parser<'a, u8, u32> {
        seq(b"#define") * macro_space()
            * seq(name) * sym(b'_') * seq(attr) * macro_space()
            * integer() - (-one_of(b"\r\n") | -end())
    }
    fn array<'a>(name: &'a [u8], attr: &'a [u8]) -> Parser<'a, u8, Vec<bool>> {
        let array = seq(b"static") * whitespace()
            * (seq(b"unsigned") * whitespace()).opt()
            // TODO: perhaps support X10 short ones?
            * seq(b"char") * whitespace()
            * seq(name) * sym(b'_') * seq(attr) * whitespace().opt()
            * sym(b'[') * whitespace().opt()
            * (integer() * whitespace().opt()).opt()
            * sym(b']') * whitespace().opt()
            * sym(b'=') * whitespace().opt()
            * sym(b'{') * whitespace().opt()
            * (
                integer() - whitespace().opt()
                + (
                    (sym(b',').opt() - whitespace().opt())
                        * integer() - whitespace().opt()
                ).repeat(..)
                - sym(b',').opt() - whitespace().opt()
            ).opt()
            - sym(b'}') - whitespace().opt()
            - sym(b';') - whitespace().opt();
        array.map(|x: Option<(u32, Vec<u32>)>| {
            match x {
                Some((a, mut v)) => {
                    v.insert(0, a);
                    v
                },
                None => Vec::new()
            }.into_iter().flat_map(|n| {
                // XBM apparently uses little endian order...
                // Also, 0 is black and 1 is white.
                // TODO: Use a built-in!
                let n = u8::try_from(n).unwrap();
                [1, 2, 4, 8, 16, 32, 64, 128].iter().map(move |m| n&m == 0)
            }).collect()
        })
    }

    pub fn parse(
        name: &str, data: &[u8]
    ) -> Result<DynamicImage, Box<dyn Error>> {
        let name: &[u8] = name.as_ref();
        let parser = define(name, b"width") - whitespace().opt()
            + define(name, b"height") - whitespace().opt()
            + array(name, b"bits");
        let ((width, height), raw) = parser.parse(data)?;
        assert_eq!(width, 32);
        assert_eq!(height, 32);
        assert_eq!(raw.len(), 32 * 32);
        Ok(DynamicImage::ImageLuma8(ImageBuffer::from_raw(
            32, 32,
            // image crate doesn't have Luma<bool> for some reason
            // probably because it's ambiguous what's black and what's white
            raw.into_iter().map(|b| if b {255} else {0}).collect()
        ).ok_or("Adding is hard, okay?")?))
    }
}
