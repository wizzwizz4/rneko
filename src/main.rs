pub mod neko;

use neko::{Sprite, NEKO};

use crow::{DrawConfig, Context, Texture};
use image::ImageBuffer;
use std::error::Error;
use std::env::set_var;
use std::time::Instant;
use winit::{
    dpi::LogicalSize,
    event::{Event, StartCause, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    window::{WindowBuilder, WindowId}
};

struct NekoTextures {
    pub left2: Texture
}
impl NekoTextures {
    fn neko_buffer(ctx: &mut Context, buffer: &[u8]) -> Texture {
        Texture::from_image(
            ctx,
            ImageBuffer::from_raw(32, 32, buffer.to_vec()).unwrap()
        ).unwrap()
    }

    pub fn from_neko(ctx: &mut Context, sprite: &Sprite) -> NekoTextures {
        NekoTextures {
            left2: NekoTextures::neko_buffer(ctx, sprite.left2)
        }
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    set_var("WINIT_X11_SCALE_FACTOR", "1");

    let event_loop = EventLoop::new();
    let mut ctx = Context::new(
        WindowBuilder::new()
            .with_inner_size(LogicalSize::new(32f64, 32f64))
            .with_resizable(false)
            .with_title("Neko!")
            .with_transparent(true)
            .with_decorations(false)
            .with_always_on_top(true),
        &event_loop
    )?;
    let id = ctx.window().id();

    let neko = NekoTextures::from_neko(&mut ctx, &NEKO);

    event_loop.run(move |event, _, control_flow| {
        match event {
            Event::NewEvents(sc) => match sc {
                StartCause::Init => *control_flow = ControlFlow::Wait,
                _ => ()
            },
            Event::WindowEvent {
                window_id, event
            } => if window_id == id {
                match event {
                    WindowEvent::CloseRequested | WindowEvent::Destroyed => {
                        *control_flow = ControlFlow::Exit
                    },
                    _ => ()
                }
            },
            Event::RedrawRequested(window_id) => if window_id == id {
                let mut surface = ctx.surface();
                ctx.clear_color(&mut surface, (0.0, 0.0, 0.0, 0.0));
                ctx.draw(
                    &mut surface, &neko.left2, (0, 0), &DrawConfig::default()
                );
                ctx.present(surface);
            },
            _ => ()
        }
    });
    unreachable!();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn valid_data() {
        let event_loop = EventLoop::new();
        let ctx = Context::new(WindowBuilder::new(), &event_loop);
        NekoTextures::from_neko(&mut ctx, NEKO);
    }
}
