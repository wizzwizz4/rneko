macro_rules! sprite_def {
    ($($name:ident)*) => {
        pub struct Sprite<'a> {
            $(
                pub $name: &'a [u8; 32 * 32 * 4],
            )*
        }
    };
}

sprite_def!(
    awake
    down1 down2
    dtogi1 dtogi2
    dwleft1 dwleft2
    dwright1 dwright2
    jare2
    kaki1 kaki2
    left1 left2
    ltogi1 ltogi2
    mati2 mati3
    right1 right2
    rtogi1 rtogi2
    sleep1 sleep2
    up1 up2
    upleft1 upleft2
    upright1 upright2
    utogi1 utogi2
);

macro_rules! load_neko {
    ($name:expr) => {
        include_bytes!(concat!(env!("OUT_DIR"), "/", $name))
    };
    ($ident:ident, $prefix:expr, $($name:ident)*) => {
        pub const $ident: Sprite<'static> = Sprite {
            $(
                $name: load_neko!(concat!($prefix, stringify!($name))),
            )*
        };
    };
    ($ident:ident, $prefix:expr) => {
        load_neko!($ident, $prefix,
            awake
            down1 down2
            dtogi1 dtogi2
            dwleft1 dwleft2
            dwright1 dwright2
            jare2
            kaki1 kaki2
            left1 left2
            ltogi1 ltogi2
            mati2 mati3
            right1 right2
            rtogi1 rtogi2
            sleep1 sleep2
            up1 up2
            upleft1 upleft2
            upright1 upright2
            utogi1 utogi2
        );
    };
}

load_neko!(NEKO, "");
